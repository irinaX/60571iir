<?php


namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class StockExchangeTablesSeeder extends Seeder
{
    private $clientsFullName = ['John Doe', 'Narek Rouben', 'Eduard Spartak', 'Almast Arevik', 'Garen Garegin'];
    private $tickers = [
        "0" => [
            "tickerName" => 'BTC',
            "tickersImgURL" => 'logos:bitcoin'
        ],
        "1" => [
            "tickerName" => 'Apple Inc.',
            "tickersImgURL" => 'logos:apple'
        ],
        "2" => [
            "tickerName" => 'PTC',
            "tickersImgURL" => 'emojione:chart-increasing-with-yen'
        ],
        "3" => [
            "tickerName" => 'S&P 500',
            "tickersImgURL" => 'emojione:chart-increasing-with-yen'
        ],
        "4" => [
            "tickerName" => 'Google',
            "tickersImgURL" => 'grommet-icons:google'
        ],
    ];
    private $orders = [
        "0" => [
            'ticker_id' => '1',
            'client_id' => '1',
            'order_type' => 'sell',
            'amount' => '45',
            'price' => '22.00',
            'date_time' => '2021-03-26 23:33:00',
        ],
        "1" => [
            'ticker_id' => '2',
            'client_id' => '1',
            'order_type' => 'buy',
            'amount' => '2',
            'price' => '1.00',
            'date_time' => '2021-03-27 18:35:44',
        ],
        "2" => [
            'ticker_id' => '4',
            'client_id' => '2',
            'order_type' => 'sell',
            'amount' => '532',
            'price' => '1000.00',
            'date_time' => '2021-03-28 20:35:25',
        ],
        "3" => [
            'ticker_id' => '3',
            'client_id' => '1',
            'order_type' => 'buy',
            'amount' => '2',
            'price' => '10.00',
            'date_time' => '2021-03-28 20:35:25',
        ],
        "4" => [
            'ticker_id' => '2',
            'client_id' => '4',
            'order_type' => 'buy',
            'amount' => '53',
            'price' => '100.00',
            'date_time' => '2021-03-28 20:35:25',
        ],
        "5" => [
            'ticker_id' => '3',
            'client_id' => '4',
            'order_type' => 'sell',
            'amount' => '54',
            'price' => '145.00',
            'date_time' => '2021-03-28 20:35:25',
        ],
    ];
    public function run()
    {
        foreach ($this->clientsFullName as $fullName) {
            $data = [
                'fullname' => $fullName,
            ];
            // Using Query Builder
            $this->db->table('client')->insert($data);
        }
        foreach ($this->tickers as $ticker) {
            $data = [
                'name' => $ticker['tickerName'],
                'img_url' => $ticker['tickersImgURL'],
            ];
            // Using Query Builder
            $this->db->table('ticker')->insert($data);
        }
        foreach ($this->orders as $order) {
            $data = [
                'ticker_id' => $order['ticker_id'],
                'client_id' => $order['client_id'],
                'order_type' => $order['order_type'],
                'amount' => $order['amount'],
                'price' => $order['price'],
                'date_time' => $order['date_time'],
            ];
            // Using Query Builder
            $this->db->table('limit_order')->insert($data);
        }
    }
}
<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="login-content">
                    <h1 class="mb-3"><?php echo lang('Auth.forgot_password_heading'); ?></h1>
                    <p class="mb-4"><?php echo sprintf(lang('Auth.forgot_password_subheading'), $identity_label); ?></p>

                    <div id="infoMessage"><?php echo $message; ?></div>

                    <?php echo form_open('auth/forgot_password'); ?>

                    <div class="mb-4">
<!--                        <label for="identity">--><?php //echo(($type === 'email') ? sprintf(lang('Auth.forgot_password_email_label'), $identity_label) : sprintf(lang('Auth.forgot_password_identity_label'), $identity_label)); ?><!--</label>-->
<!--                        <br/>-->
<!--                        --><?php //echo form_input($identity); ?>
                        <?php echo form_input($identity, '', 'class="form-control" placeholder="Email"'); ?>

                    </div>
                    <?php echo form_submit('submit', lang('Auth.forgot_password_submit_btn'), 'class="btn btn-login w-100"'); ?>
<!--                    <div>--><?php //echo form_submit('submit', lang('Auth.forgot_password_submit_btn')); ?><!--</div>-->

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>


<?= $this->endSection() ?>
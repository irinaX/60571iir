<?= $this->extend('templates/dashboard_layout') ?>
<?= $this->section('title') ?>
<?= $this->renderSection('title') ?>
<?php use CodeIgniter\I18n\Time; ?>
<?php if (!empty($order)) : ?>
    Order <?= esc($order['id']); ?>
<?php endif ?>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
    <section class="order">
        <div class="container">
            <?php if (!empty($order)) : ?>
                <div class="order-item">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-2">
                            <div class=""><span class="iconify" data-icon="logos:bitcoin"
                                                data-inline="false"></span><span>Bitcoin</span></div>
                            <div class="order-item__type"><?= esc($order['order_type']); ?></div>
                        </div>
                        <div class="col-md-6 col-lg-2">
                            <div class=""><span>Ticker ID: </span><?= esc($order['ticker_id']); ?></div>
                            <div class=""><span>Client ID: </span><?= esc($order['client_id']); ?></div>
                        </div>
                        <div class="col-md-6 col-lg-2">
                            <div class=""><span>Amount: </span><?= esc($order['amount']); ?></div>
                            <div class=""><span>Price: </span><?= esc($order['price']); ?>$</div>
                        </div>
                        <div class="col-md-6 col-lg-3"><span>Ordered: </span><?= esc($order['date_time']); ?></div>
                        <div class="col-md-6 col-lg-3">
                            <a href="<?= base_url() ?>/order/edit/<?= esc($order['id']); ?>"
                               class="btn btn-secondary">Edit</a>
                            <a href="<?= base_url() ?>/order/delete/<?= esc($order['id']); ?>"
                               class="btn btn-secondary">Delete</a></div>
                    </div>
                </div>
            <?php else : ?>
                <p>No info found.</p>
            <?php endif ?>
        </div>
    </section>
<?= $this->endSection() ?>
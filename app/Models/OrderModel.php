<?php


namespace App\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
    protected $table = 'limit_order'; //таблица, связанная с моделью
    protected $primaryKey = 'id';
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'ticker_id', 'client_id', 'order_type', 'amount', 'price', 'date_time'];

    public function getOrder($id = null, $per_page = null, $search = '')
    {
        if (!isset($id)) {
            $builder = $this->select('*, limit_order.id')
                ->join('client', 'limit_order.client_id = client.id', 'LEFT')
                ->join('ticker', 'limit_order.ticker_id = ticker.id', 'LEFT')
                ->like('name', $search, 'both', null, true)
                ->orlike('fullname', $search, 'both', null, true);
            return $builder->paginate($per_page, 'group1');
        } else {
            $builder = $this->where('limit_order.id', $id)->first();
            return $builder;
        }
    }

    public function getOrderWithType($type = null, $per_page = null, $search = '')
    {
        $this->select('*, limit_order.id')
            ->join('client', 'limit_order.client_id = client.id', 'LEFT')
            ->join('ticker', 'limit_order.ticker_id = ticker.id', 'LEFT')
            ->like('name', $search, 'both', null, true)
            ->where('order_type', $type)
            ->orlike('fullname', $search, 'both', null, true)
            ->where('order_type', $type);
        return $this->paginate($per_page, 'group1');
    }
}

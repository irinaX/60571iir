<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-7 col-xl-6">
            <div class="login-content">
                <h1 class="mb-3 login__title"><?php echo lang('Auth.login_heading'); ?></h1>
                <p class="mb-4"><?php echo lang('Auth.login_subheading'); ?></p>
                <?php if (isset($message)): ?>
                    <div class="alert alert-info" role="alert" >
                        <?php echo $message; ?>
                    </div>
                <?php endif ?>
                <?php echo form_open('auth/login'); ?>
                <div class="mb-3">
                    <?php echo form_input($identity, '', 'class="form-control" value="Mark" placeholder="' . trim(lang('Auth.login_identity_label'), ":") . '"'); ?>
                </div>
                <div class="mb-4">
                    <?php echo form_input($password, '', 'class="form-control" value="Mark" placeholder="' . trim(lang('Auth.login_password_label'), ":") . '"'); ?>
                </div>
                <div class="mb-4 login-content__checkbox">
                    <?php echo form_checkbox('remember', '1', false, 'id="remember" class="custom-checkbox"'); ?>
                    <?php echo form_label(lang('Auth.custom_login_remember_label'), 'remember'); ?>
                </div>
                <div class="mb-4">
                    <?php echo form_submit('submit', lang('Auth.custom_login_submit_btn'), 'class="btn btn-login w-100"'); ?>
                </div>
                <?php echo form_close(); ?>
                <div class="login-content__footer">
                    <div class="mb-3">
                        Don’t have an account? <a
                                href="register_user"><?php echo lang('Auth.register_user_link'); ?></a>
                    </div>
                    <a href="forgot_password" class="d-block mb-4"><?php echo lang('Auth.login_forgot_password'); ?></a>
                    <a href="<?= $authUrl; ?>" class="btn btn-outline-primary" role="button" style="text-transform:none">
                        <img alt="Google sign-in" width="20"
                             src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
                        <?= lang('XTrade.login_with_google') ?>
                    </a>
                </div>

            </div>

        </div>
    </div>
</div>

<?= $this->endSection() ?>

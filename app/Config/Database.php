<?php

namespace Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class Database extends Config
{
    /**
     * The directory that holds the Migrations
     * and Seeds directories.
     *
     * @var string
     */
    public $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

    /**
     * Lets you choose which connection group to
     * use if no other is specified.
     *
     * @var string
     */
    public $defaultGroup = 'default';

    /**
     * The default database connection.
     *
     * @var array
     */
//public $default = [
//    'DSN' => '',
//    'hostname' => '',
//    'username' => '',
//    'password' => '',
//    'database' => '',
//    'DBDriver' => 'MySQLi',
//    'DBPrefix' => '',
//    'pConnect' => false,
//    'DBDebug' => (ENVIRONMENT !== 'production'),
//    'charset' => 'utf8',
//    'DBCollat' => 'utf8_general_ci',
//    'swapPre' => '',
//    'encrypt' => false,
//    'compress' => false,
//    'strictOn' => false,
//    'failover' => [],
//    'port' => 3306,
//];

//        'DSN' => 'Postgre:host=ec2-54-73-68-39.eu-west-1.compute.amazonaws.com;port=5432;dbname=d72b4l0q9m7i7n;user=bynsmfsvsysqxq;password=3469cfe03748d17909fdc26b88350a3ad1c12675460bba91f00234fb64d9bfa9',

    public $default = [
        'DSN'      => '',
        'hostname' => 'ec2-54-73-68-39.eu-west-1.compute.amazonaws.com',
        'username' => 'bynsmfsvsysqxq',
        'password' => '3469cfe03748d17909fdc26b88350a3ad1c12675460bba91f00234fb64d9bfa9',
        'database' => 'd72b4l0q9m7i7n',
        'DBDriver' => 'Postgre',
        'DBPrefix' => '',
        'pConnect' => false,
        'DBDebug' => (ENVIRONMENT !== 'production'),
        'charset' => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'swapPre' => '',
        'encrypt' => false,
        'compress' => false,
        'strictOn' => false,
        'failover' => [],
        'port' => 5432,
    ];

    /**
     * This database connection is used when
     * running PHPUnit database tests.
     *
     * @var array
     */
    public $tests = [
        'DSN' => '',
        'hostname' => '',
        'username' => '',
        'password' => '',
        'database' => '',
        'DBDriver' => 'MySQLi',
        'DBPrefix' => '',
        'pConnect' => false,
        'DBDebug' => (ENVIRONMENT !== 'production'),
        'charset' => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'swapPre' => '',
        'encrypt' => false,
        'compress' => false,
        'strictOn' => false,
        'failover' => [],
        'port' => 3306,
    ];

    //--------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        // Ensure that we always set the database group to 'tests' if
        // we are currently running an automated test suite, so that
        // we don't overwrite live data on accident.
        if (ENVIRONMENT === 'testing') {
            $this->defaultGroup = 'tests';
        }
    }

    //--------------------------------------------------------------------

}

<?= $this->extend('templates/dashboard_layout') ?>
<?= $this->section('title') ?>
<?= $this->renderSection('title') ?>
    Orders
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<?= $this->renderSection('content') ?>
    <section class="orders">
        <div class="container">
            <div class="orders__header">
                <div class="orders__header-btn-group">
                    <?php if (count($allOrders) !== 0) : ?>
                        <div class="mr-4">
                            Total: <?= count($allOrders) ?>
                        </div>
                    <?php endif; ?>
                    <!--                    <button type="button" class="btn btn-filter ">All</button>-->
                    <a href="<?= base_url() ?>/order"
                       class="btn btn-filter <?= current_url() === base_url() . '/order' ? 'active' : ''; ?>">All</a>
                    <a href="<?= base_url() ?>/order/viewAllWithType/Buy"
                       class="btn btn-filter <?= current_url() === base_url() . '/order/viewAllWithType/Buy' ? 'active' : ''; ?>">Buy</a>
                    <a href="<?= base_url() ?>/order/viewAllWithType/Sell"
                       class="btn btn-filter <?= current_url() === base_url() . '/order/viewAllWithType/Sell' ? 'active' : ''; ?>">Sell</a>
                </div>
                <div>
                    <a href="<?= base_url() ?>/order/store" class="btn btn-outline-primary">
                        Add new order
                    </a>
                </div>

            </div>
            <?php if (!empty($order) && is_array($order)) : ?>
                <?php foreach ($order as $item): ?>
                    <div class="orders-item">
                        <div class="row orders-item__wrap">
                            <div class="col-md-3 orders-item__logo mb-2 mb-md-0">
                                <span class="iconify icon-logo" data-icon="<?= esc($item["img_url"]); ?>"
                                      data-inline="false"></span>
                                <?= esc($item['name']); ?>
                            </div>
                            <div class="col-md-1 mb-2 mb-md-0">
                                <div class="<?= esc($item['order_type']) === 'buy' ? 'orders-item__buy' : 'orders-item__sell'; ?> ">
                                    <?= esc($item['order_type']); ?>
                                </div>
                            </div>
                            <div class="offset-md-1 col-md-2 mb-2 mb-md-0">
                                Amount: <?= esc($item['amount']); ?>
                            </div>
                            <div class="col-md-2 px-md-0 mb-2 mb-md-0">
                                <?= esc($item['date_time']); ?>
                            </div>
                            <div class="col-md-3 orders-item__price">
                                <div class="mr-3 <?= esc($item['order_type']) === 'buy' ? 'orders-item__price-buy' : 'orders-item__price-sell'; ?>">
                                    $<?= esc($item['price']); ?>
                                    <!--                                    item: --><?php //var_dump($item) ?>
                                </div>
                                <!--                                <a href="-->
                                <? //= base_url() ?><!--/index.php/order/view/--><? //= esc($item['id']); ?><!--"-->
                                <!--                                   class="btn btn-secondary">Show</a>-->
                                <button type="button" class="btn btn-open-menu py-0 pl-0 pr-0">
                                    <span class="iconify icon-dots" data-icon="ph:dots-three-bold"
                                          data-inline="false"></span>
                                    <span class="orders-item__menu">
                                        <a href="<?= base_url() ?>/order/edit/<?= esc($item['id']); ?>">
                                            <span class="iconify" data-icon="akar-icons:edit"
                                                  data-inline="false"></span> Edit</a>
                                        <a href="<?= base_url() ?>/order/delete/<?= esc($item['id']); ?>">
                                            <span class="iconify" data-icon="carbon:delete"
                                                  data-inline="false"></span> Delete</a>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <p>No info found.</p>
            <?php endif ?>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1', 'my_page') ?>
                <?= form_open('order'); ?>
                <div class="input-group">
                    <select name="per_page" aria-label="per_page" class="custom-select" id="inputGroupSelect04">
                        <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                        <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                        <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                        <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-primary" type="submit">on page</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
<?= $this->endSection() ?>

<?php //if (session()->getFlashdata('message')) : ?>
<!--    <div class="alert alert-info" role="alert" style="position: fixed;right: 0;bottom: 0;margin: 0;">-->
<!--        --><?//= session()->getFlashdata('message') ?>
<!--    </div>-->
<?php //endif ?>

<?= $this->extend('templates/layout');
echo strlen($message) ?>
<?= $this->section('content') ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-7 col-xl-6">
                <div class="login-content">
                    <h1><?php echo lang('Auth.register_user_heading'); ?></h1>
                    <p class="mb-4"><?php echo lang('Auth.create_user_subheading'); ?></p>
                    <?php if (isset($message)): ?>
                        <?php echo $message; ?>
                    <?php endif ?>
                    <?php echo form_open('auth/register_user'); ?>
                    <div class="mb-3">
                        <?php echo form_input($first_name, '', 'class="form-control" placeholder="' . trim(lang('Auth.create_user_fname_label'), ":") . '"'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_input($last_name, '', 'class="form-control" placeholder="' . trim(lang('Auth.create_user_lname_label'), ":") . '"'); ?>
                    </div>
                    <?php
                    if ($identity_column !== 'email') {
                        echo '<div class="mb-3">';
                        echo form_label(lang('Auth.create_user_identity_label'), 'identity');
                        echo '<br/>';
                        echo \Config\Services::validation()->getError('identity');
                        echo form_input($identity);
                        echo '</div>';
                    }
                    ?>
                    <div class="mb-3">
                        <?php echo form_input($email, '', 'class="form-control" placeholder="' . trim(lang('Auth.create_user_email_label'), ":") . '"'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_input($password, '', 'class="form-control" placeholder="' . trim(lang('Auth.create_user_password_label'), ":") . '"'); ?>
                    </div>
                    <div class="mb-4">
                        <?php echo form_input($password_confirm, '', 'class="form-control" placeholder="' . trim(lang('Auth.create_user_password_confirm_label'), ":") . '"'); ?>
                    </div>
                    <div class="mb-3">
                    <?php echo form_submit('submit', lang('Auth.register_user_submit_btn'), 'class="btn btn-login"'); ?>
                    </div>
                    <?php echo form_close(); ?>
                    <div class="login-content__footer">
                        Already have an account? <a
                                href="login"><?php echo lang('Auth.login_user_link'); ?></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?= $this->endSection() ?>
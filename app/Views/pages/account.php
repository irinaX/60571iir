<?= $this->extend('templates/dashboard_layout') ?>
<?= $this->section('title') ?>
<?= $this->renderSection('title') ?>
    Account
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<?= $this->renderSection('content') ?>
    <section class="bd-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4 mb-4 mb-md-0">
                    <div class="account__info">
                        <?php if ($ionAuth->user()->row()->avatar_url !== null): ?>
                            <img class="client-item__img mr-3"
                                 src="<?php echo $ionAuth->user()->row()->avatar_url; ?>"
                                 alt="">
                        <?php else: ?>
                            <img class="client-item__img mr-3"
                                 src="https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
                                 alt="">
                        <?php endif; ?>
                        <div class="mr-4">
                            <div class="client-item__name">
                                <?php echo $ionAuth->user()->row()->first_name; ?>
                                <?php echo $ionAuth->user()->row()->last_name; ?></div>
                            <div class="client-item__time"><?php echo $ionAuth->user()->row()->email; ?></div>
                            <!--                            Last update 21:14:13, 12 June 2020-->
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="client-item__label">Balance</div>
                    <div class="client-item__val">$<?php echo $ionAuth->user()->row()->balance; ?></div>
                </div>
                <div class="col-12">
                    <?php $hiddenFields = array('id' => $ionAuth->user()->row()->id); ?>
                    <?= form_open_multipart('account/setAvatar', '', $hiddenFields); ?>
                    <div class="form-group mt-3 mb-4">
                        <input type="file" id="avatar"
                               class="inputfile <?= ($validation->hasError('avatar_url')) ? 'is-invalid' : ''; ?>"
                               name="avatar_url">
                        <label class="btn btn-filter mb-0 pl-0" for="avatar">
                            <span class="iconify" data-icon="entypo:upload" data-inline="false"></span>
                            Change avatar
                        </label>
                        <button type="submit" class="btn btn-outline-primary d-none" name="btn-change-avatar">Ok
                        </button>
                        <div class="invalid-feedback">
                            <?= $validation->getError('avatar_url') ?>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?= $this->endSection() ?>
<?php


namespace App\Controllers;

use App\Models\ClientModel;
use App\Models\OrderModel;
use App\Models\TickerModel;
use CodeIgniter\Model;

class Order extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new OrderModel();
        $data['allOrders'] = $model->getOrder();
        $data ['order'] = $model->getOrder(null, $per_page, $search);
        $data['pager'] = $model->pager;
        echo view('order/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new OrderModel();
        $data ['order'] = $model->getOrder($id);
        echo view('order/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $ticker_model = new TickerModel();
        $client_model = new ClientModel();
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $data ['ticker'] = $ticker_model->getTicker();
        $data ['client'] = $client_model->getClient();
        echo view('order/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
//                'id'=> 'required',
                'ticker_id' => 'required',
                'client_id' => 'required',
                'order_type' => 'required',
                'amount' => 'required',
                'price' => 'required',
                'date_time' => 'required',
            ])) {
            $model = new OrderModel();
            $model->save([
//                'id'=> $this->request->getPost('id'),
                'ticker_id' => $this->request->getPost('ticker_id'),
                'client_id' => $this->request->getPost('client_id'),
                'order_type' => $this->request->getPost('order_type'),
                'amount' => $this->request->getPost('amount'),
                'price' => $this->request->getPost('price'),
                'date_time' => $this->request->getPost('date_time'),
            ]);
            session()->setFlashdata('message', lang('XTrade.order_create_success'));
            if ($this->ionAuth->isAdmin()) {
                return redirect()->to('/order/viewAllWithClients');
            } else {
                return redirect()->to('/order');
            }
        } else {
            return redirect()->to('/order/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new OrderModel();
        $ticker_model = new TickerModel();
        $client_model = new ClientModel();

        helper(['form']);
        $data ['order'] = $model->getOrder($id);
        $data ['validation'] = \Config\Services::validation();
        $data ['ticker'] = $ticker_model->getTicker();
        $data ['client'] = $client_model->getClient();
        echo view('order/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/order/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'ticker_id' => 'required',
                'client_id' => 'required',
                'order_type' => 'required',
                'amount' => 'required',
                'price' => 'required',
                'date_time' => 'required',
            ])) {
            $model = new OrderModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'ticker_id' => $this->request->getPost('ticker_id'),
                'client_id' => $this->request->getPost('client_id'),
                'order_type' => $this->request->getPost('order_type'),
                'amount' => $this->request->getPost('amount'),
                'price' => $this->request->getPost('price'),
                'date_time' => $this->request->getPost('date_time'),
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

//            return redirect()->to('/order/view/'.$this->request->getPost('id'));
            if ($this->ionAuth->isAdmin()) {
                return redirect()->to('/order/viewAllWithClients');
            } else {
                return redirect()->to('/order');
            }

        } else {
            return redirect()->to('/order/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new OrderModel();
        $model->delete($id);
        if ($this->ionAuth->isAdmin()) {
            return redirect()->to('/order/viewAllWithClients');
        } else {
            return redirect()->to('/order');
        }
    }

    public function viewAllWithClients()
    {
        if ($this->ionAuth->isAdmin()) {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form', 'url']);
            $model = new OrderModel();
//            $pager = \Config\Services::pager();
//            $data['order'] = $model->getOrder()->paginate($per_page, 'group1');
//            $data['pager'] = $model->pager;
            $data['allOrders'] = $model->getOrder();
            $data['order'] = $model->getOrder(null, $per_page, $search);
            $data['pager'] = $model->pager;
            echo view('order/view_all_with_clients', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('XTrade.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function viewAllWithType($type)
    {

        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form', 'url']);
        $model = new OrderModel();
//        $pager = \Config\Services::pager();
        $data['allOrders'] = $model->getOrderWithType(strtolower($type), null, $search);
        $data['order'] = $model->getOrderWithType(strtolower($type), $per_page, $search);
        $data['pager'] = $model->pager;
        if ($this->ionAuth->isAdmin()) {
            echo view('order/view_all_with_clients', $this->withIon($data));
        } else {
            echo view('order/view_all', $this->withIon($data));
        }

    }


}
<?php


namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class IonAuthTablesSeeder extends Seeder
{
    //--- code from vendor/benedmunds/codeigniter-ion-auth/Database/Seeds/IonAuthSeeder.php ---

    public function run()
    {
        $data = [
            'name' => 'admin',
            'description' => 'Administrator',
        ];
        $this->db->table('groups')->insert($data);

        $data = [
            'name' => 'members',
            'description' => 'General User',
        ];

        $this->db->table('groups')->insert($data);

        $data = [
            'ip_address' => '127.0.0.1',
            'username' => 'administrator',
            'password' => '$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',
            'email' => 'admin@admin.com',
            'activation_selector' => '',
            'activation_code' => '',
            'forgotten_password_selector' => NULL,
            'forgotten_password_code' => NULL,
            'forgotten_password_time' => NULL,
            'remember_selector' => NULL,
            'remember_code' => NULL,
            'created_on' => '1268889823',
            'last_login' => '1268889823',
            'active' => '1',
            'first_name' => 'Admin',
            'last_name' => 'istrator',
            'company' => 'ADMIN',
            'phone' => '0',
            'avatar_url' => null,
            'balance' => 10000,
        ];
        $this->db->table('users')->insert($data);
        $data = [
            'ip_address' => '127.0.0.1',
            'username' => 'qwer@qwer.ru',
            'password' => '$2y$10$Jc7chbCZIO/qKZXwDKjew.umAv0w8HVBPVUZRyxX8vwqir9F/CDQ2',
            'email' => 'qwer@qwer.ru',
            'activation_selector' => NULL,
            'activation_code' => NULL,
            'forgotten_password_selector' => NULL,
            'forgotten_password_code' => NULL,
            'forgotten_password_time' => NULL,
            'remember_selector' => NULL,
            'remember_code' => NULL,
            'created_on' => '1618215327',
            'last_login' => '1618215327',
            'active' => '1',
            'first_name' => 'qwer',
            'last_name' => 'qwer',
            'company' => null,
            'phone' => null,
            'avatar_url' => null,
            'balance' => 10000,
        ];
        $this->db->table('users')->insert($data);

        $data = [
            'user_id' => 1,
            'group_id' => 1,
        ];

        $this->db->table('users_groups')->insert($data);

        $data = [
            'user_id' => 1,
            'group_id' => 2,
        ];

        $this->db->table('users_groups')->insert($data);
        $data = [
            'user_id' => 2,
            'group_id' => 2,
        ];
        $this->db->table('users_groups')->insert($data);
    }
}

-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 24, 2021 at 04:36 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stock_exchange`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int NOT NULL COMMENT 'id счета',
  `client_id` int NOT NULL COMMENT ' id клиента',
  `price` decimal(10,2) NOT NULL COMMENT 'сумма',
  `date_time` datetime NOT NULL COMMENT 'дата и время'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `client_id`, `price`, `date_time`) VALUES
(1, 1, '123456.78', '2021-02-24 13:31:05'),
(2, 2, '12345.67', '2021-02-24 14:31:05'),
(3, 3, '1234.56', '2021-02-24 15:31:05'),
(4, 4, '500000.00', '2021-02-24 16:25:56'),
(5, 5, '40000.00', '2021-02-24 16:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int NOT NULL COMMENT 'id клиента',
  `fullname` varchar(255) NOT NULL COMMENT 'ФИО'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `fullname`) VALUES
(1, 'Иванов И.И.'),
(2, 'Петров П.П.'),
(3, 'Сидоров С.С.'),
(4, 'Жукова Н.В.'),
(5, 'Козловская С.Т.');

-- --------------------------------------------------------

--
-- Table structure for table `limit_order`
--

CREATE TABLE `limit_order` (
  `id` int NOT NULL COMMENT 'id заявки',
  `ticker_id` int NOT NULL COMMENT 'id тикера',
  `client_id` int NOT NULL COMMENT 'id клиента',
  `order_type` varchar(255) NOT NULL COMMENT 'тип заявки (покупка/продажа)',
  `amount` int NOT NULL COMMENT 'количество',
  `price` decimal(10,2) NOT NULL COMMENT 'цена',
  `date_time` datetime NOT NULL COMMENT 'дата и время'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `limit_order`
--

INSERT INTO `limit_order` (`id`, `ticker_id`, `client_id`, `order_type`, `amount`, `price`, `date_time`) VALUES
(1, 1, 1, 'sell', 10, '50465.31', '2021-02-24 15:53:49'),
(2, 1, 2, 'buy', 10, '50000.00', '2021-02-24 15:53:49'),
(3, 2, 2, 'sell', 5, '125.30', '2021-02-24 15:59:49'),
(4, 4, 3, 'buy', 15, '3800.00', '2021-02-24 15:59:49'),
(5, 5, 3, 'buy', 500, '260.00', '2021-02-24 16:01:42'),
(6, 2, 2, 'sell', 50, '130.00', '2021-02-24 16:01:42'),
(7, 4, 1, 'buy', 25, '3870.00', '2021-02-24 16:03:11'),
(8, 3, 3, 'buy', 30, '1450.00', '2021-02-24 16:03:11'),
(9, 5, 2, 'sell', 100, '300.00', '2021-02-24 16:04:31'),
(10, 3, 2, 'sell', 230, '1500.00', '2021-02-24 16:04:31'),
(11, 3, 2, 'sell', 320, '1480.00', '2021-02-24 16:05:59'),
(12, 4, 1, 'buy', 100, '3700.00', '2021-02-24 16:05:59'),
(13, 2, 2, 'buy', 500, '100.00', '2021-02-24 16:07:06'),
(14, 5, 2, 'buy', 800, '250.00', '2021-02-24 16:07:06'),
(15, 5, 2, 'sell', 1000, '150.00', '2021-02-24 16:08:24'),
(21, 1, 4, 'sell', 2, '50500.00', '2021-02-23 16:30:24'),
(22, 2, 5, 'buy', 500, '120.00', '2021-02-24 16:31:17'),
(23, 3, 4, 'sell', 8, '1500.00', '2021-02-24 16:31:17'),
(24, 4, 5, 'sell', 3, '4000.00', '2021-02-24 16:31:17'),
(25, 5, 5, 'sell', 1000, '290.00', '2021-02-24 16:31:17');

-- --------------------------------------------------------

--
-- Table structure for table `ticker`
--

CREATE TABLE `ticker` (
  `id` int NOT NULL COMMENT 'id тикера',
  `name` varchar(255) NOT NULL COMMENT 'наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticker`
--

INSERT INTO `ticker` (`id`, `name`) VALUES
(1, 'BTC'),
(2, 'Apple Inc.'),
(3, 'PTC'),
(4, 'S&P 500'),
(5, 'Сбербанк');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int NOT NULL COMMENT 'id сделки',
  `ticker_id` int NOT NULL COMMENT 'id тикера',
  `client_id` int NOT NULL COMMENT 'id клиента',
  `transaction_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'тип сделки (покупка/продажа)',
  `amount` int NOT NULL COMMENT 'количество',
  `price` decimal(10,2) NOT NULL COMMENT 'цена',
  `date_time` datetime NOT NULL COMMENT 'дата и время'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `ticker_id`, `client_id`, `transaction_type`, `amount`, `price`, `date_time`) VALUES
(11, 1, 1, 'buy', 1, '50425.50', '2021-02-24 16:14:08'),
(12, 3, 2, 'sell', 20, '1462.70', '2021-02-24 16:14:08'),
(13, 2, 3, 'sell', 10, '125.50', '2021-02-24 16:15:47'),
(14, 5, 1, 'sell', 135, '271.60', '2021-02-24 16:15:47'),
(15, 1, 2, 'sell', 2, '50420.60', '2021-02-24 16:15:47'),
(16, 4, 1, 'buy', 10, '3881.30', '2021-02-24 16:15:47'),
(17, 3, 3, 'buy', 20, '1462.30', '2021-02-24 16:15:47'),
(18, 2, 1, 'sell', 50, '125.50', '2021-02-24 16:15:47'),
(19, 5, 2, 'buy', 500, '271.00', '2021-02-24 16:15:47'),
(20, 1, 3, 'buy', 1, '50263.00', '2021-02-24 16:15:47'),
(21, 4, 4, 'sell', 420, '3880.00', '2021-02-24 16:26:50'),
(22, 1, 5, 'sell', 5, '50250.50', '2021-02-23 16:26:50'),
(23, 3, 4, 'buy', 10, '1460.00', '2021-02-24 16:27:25'),
(24, 2, 5, 'buy', 124, '120.00', '2021-02-22 16:27:25'),
(25, 5, 4, 'buy', 789, '250.00', '2021-02-17 16:27:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limit_order`
--
ALTER TABLE `limit_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticker_id` (`ticker_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `ticker`
--
ALTER TABLE `ticker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticker_id` (`ticker_id`),
  ADD KEY `client_id` (`client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id счета', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id клиента', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `limit_order`
--
ALTER TABLE `limit_order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id заявки', AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ticker`
--
ALTER TABLE `ticker`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id тикера', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id сделки', AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `limit_order`
--
ALTER TABLE `limit_order`
  ADD CONSTRAINT `limit_order_ibfk_1` FOREIGN KEY (`ticker_id`) REFERENCES `ticker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `limit_order_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`ticker_id`) REFERENCES `ticker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<head>
    <title>XTrade</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap"
          rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800&display=swap"
          rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/styles/style.css"/>
</head>
<body class="db-body">
<nav class="db-nav">
    <ul class="db-nav__list">
        <li class="db-nav__list-item">
            <?php use IonAuth\Libraries\IonAuth;

            $ionAuth = new IonAuth();
            ?>
            <?php if ($ionAuth->isAdmin()): ?>
                <a href="<?= base_url() ?>/order/viewAllWithClients"
                   class="<?= current_url() === base_url() . '/order/viewAllWithClients' ? 'active' : ''; ?>"
                >
                    <span class="iconify" data-icon="fe:list-order" data-inline="false"></span>
                </a>
            <?php else: ?>
                <a href="<?= base_url() ?>/order"
                   class="<?= current_url() === base_url() . '/order' ? 'active' : ''; ?>"
                >
                    <span class="iconify" data-icon="fe:list-order" data-inline="false"></span>
                </a>
            <?php endif ?>
        </li>
        <li class="db-nav__list-item">
            <a href="<?= base_url() ?>/account"
               class="<?= current_url() === base_url() . '/account' ? 'active' : ''; ?>"
            ><span class="iconify" data-icon="codicon:account" data-inline="false"></span>
            </a>
        </li>
    </ul>
    <div class="db-nav__list-item db-nav__logout">
        <a href="<?= base_url() ?>/auth/logout"><span class="iconify" data-icon="ic:twotone-logout"
                                                      data-inline="false"></span></a>
    </div>
</nav>
<main role="main">
    <section class="bd-header">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-3"><h2><?= $this->renderSection('title') ?></h2></div>
                <div class="d-none d-lg-block col-5 text-right">
                    <ul class="bd-header__links">
                        <li class="bd-header__links-item"><a href="#">Support Team</a></li>
                        <li class="bd-header__links-item"><a href="<?php echo base_url(); ?>/pages/view/agreement">Privacy
                                Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?= $this->renderSection('content') ?>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
<script src="/app.js"></script>
</body>
</html>
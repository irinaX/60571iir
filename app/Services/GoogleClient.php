<?php


namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('947532426035-iv1ovgj18l08veegs6jvpvcbcerei9to.apps.googleusercontent.com');
        $this->google_client->setClientSecret('vaEW9BsoRYEdDK9xSWR525MG');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}
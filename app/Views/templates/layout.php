<!DOCTYPE html>
<head>
    <title>XTrade</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap"
          rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/styles/style.css"/>
</head>
<body>
<header class="header">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <button class="order-1 order-lg-0 navbar-toggler p-0" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><span class="iconify" data-icon="eva:menu-fill"
                                                        data-inline="false"></span></span>
            </button>
            <div class="order-1 order-lg-0 collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link p-lg-0" href="<?= base_url() ?>/order">Dashboard</a>
                    </li>
                    <!--                    <li class="nav-item">-->
                    <!--                        <a class="nav-link p-lg-0" href="-->
                    <? //= base_url() ?><!--/order">Orders</a>-->
                    <!--                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link p-lg-0" href="<?php echo base_url(); ?>/pages/view/agreement">
                            Privacy policy
                        </a>
                    </li>
                </ul>
            </div>
            <a class="order-0 order-lg-1 navbar-brand" href="<?= base_url() ?>"><span>X</span>Trade</a>
        </nav>
    </div>
</header>
<main role="main">
    <?= $this->renderSection('content') ?>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
</body>
</html>
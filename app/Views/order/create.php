<?= $this->extend('templates/dashboard_layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <h2 class="mb-4">Create new order</h2>
        <?= form_open_multipart('order/store'); ?>
        <div class="form-group">
                <label class="form-check-label mb-2" for="create_ticker_name_list">Ticker</label>
                <input type="hidden"
                       id="create_ticker_input"
                       name="ticker_id"
                       value="<?= old('ticker_id'); ?>"
                >
                <select class="form-select <?= ($validation->hasError('ticker_id')) ? 'is-invalid' : ''; ?>"
                        id="create_ticker_name_list"
                >
                    <option selected>Choose...</option>
                    <?php foreach ($ticker as $item): ?>
                        <option value="<?= esc($item['id']); ?>">
                            <?= esc($item['name']); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('ticker_id') ?>
            </div>
        </div>
        <div class="form-group">
                <label class="form-check-label mb-2" for="create_client_name_list">Client</label>
                <input type="hidden"
                       id="create_client_input"
                       name="client_id"
                       value="<?= old('client_id'); ?>"
                >
                <select class="form-select <?= ($validation->hasError('client_id')) ? 'is-invalid' : ''; ?>"
                        id="create_client_name_list"
                >
                    <option selected>Choose...</option>
                    <?php foreach ($client as $item): ?>
                        <option value="<?= esc($item['id']); ?>">
                            <?= esc($item['fullname']); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('client_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="form-check-label mb-2">Type:</label>
            <div class="form-check ">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="order_type"
                           value="buy" <?= old('order_type') == 'buy' ? 'checked' : '' ?> >

                    <small class="form-text">buy</small>
                </label>
            </div>
            <div class="form-check ">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="order_type"
                           value="sell" <?= old('order_type') == 'sell' ? 'checked' : '' ?> >

                    <small class="form-text">sell</small>
                </label>
            </div>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('order_type') ?>
            </div>
        </div>
        <div class="form-group">
            <!--            <label for="amount">amount</label>-->
            <input type="text" class="form-control <?= ($validation->hasError('amount')) ? 'is-invalid' : ''; ?>"
                   name="amount"
                   value="<?= old('amount'); ?>" placeholder="Amount">
            <div class="invalid-feedback">
                <?= $validation->getError('amount') ?>
            </div>
        </div>
        <div class="form-group">
            <!--            <label for="price">price</label>-->
            <input type="text" class="form-control <?= ($validation->hasError('price')) ? 'is-invalid' : ''; ?>"
                   name="price"
                   value="<?= old('price'); ?>" placeholder="Price">
            <div class="invalid-feedback">
                <?= $validation->getError('price') ?>
            </div>
        </div>
        <div class="form-group mb-4">
            <!--            <label for="date_time">date_time</label>-->
            <input type="datetime-local"
                   class="form-control <?= ($validation->hasError('date_time')) ? 'is-invalid' : ''; ?>"
                   name="date_time" value="<?= old('date_time'); ?>" placeholder="2021-06-12T19:30">
            <div class="invalid-feedback">
                <?= $validation->getError('date_time') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-secondary d-block mx-auto" name="submit">Create</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>

<?php
/*    <div class="form-group">
        <!--            <label for="ticker_id">ticker_id</label>-->
        <input type="text" class="form-control <?= ($validation->hasError('ticker_id')) ? 'is-invalid' : ''; ?>"
               name="ticker_id"
               value="<?= old('ticker_id'); ?>" placeholder="Ticker ID">
        <div class="invalid-feedback">
            <?= $validation->getError('ticker_id') ?>
        </div>
    </div>
    <div class="form-group mb-4">
        <!--            <label for="client_id">client_id</label>-->
        <input type="text" class="form-control <?= ($validation->hasError('client_id')) ? 'is-invalid' : ''; ?>"
               name="client_id"
               value="<?= old('client_id'); ?>" placeholder="Client ID">
        <div class="invalid-feedback">
            <?= $validation->getError('client_id') ?>
        </div>
    </div>*/
?>
(function () {
    console.log('App is working.');
    let btn = document.querySelector("button[name='btn-change-avatar']");
    let inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        let label = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function (e) {
            let fileName = '',
                letterNumber = null,
                labelText = '';
            fileName = e.target.value;
            if (fileName) {
                for (let i = fileName.length - 1; fileName[i] !== '\\'; i--) {
                    letterNumber = i;
                }
                for (let i = letterNumber; i < fileName.length; i++) {
                    labelText = labelText + fileName[i];
                }
                label.innerHTML = labelText;
                btn.classList.remove('d-none');
            } else {
                label.innerHTML = labelVal;
                btn.classList.add('d-none');
            }
        });
    });
}());
(function () {
    bind(document.getElementById('create_ticker_input'), document.querySelector('#create_ticker_name_list'));
    bind(document.getElementById('create_client_input'), document.querySelector('#create_client_name_list'));
    bind(document.getElementById('edit_ticker_input'), document.querySelector('#edit_ticker_name_list'), true);
    bind(document.getElementById('edit_client_input'), document.querySelector('#edit_client_name_list'), true);

    function bind(inputItem, list, twoWay = false) {
        let inputItemVar = inputItem;
        let namesList = list;
        if (inputItemVar && twoWay) {
            namesList.value = inputItemVar.value;
        }
        if (namesList) {
            namesList.addEventListener('click', eventHandler);
        }
        function eventHandler(event) {
            inputItemVar.value = namesList.value;
        }
    }
}());



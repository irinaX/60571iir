<?php


namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'first_name', 'last_name', 'avatar_url', 'balance'];

    public function getUser($id)
    {
        $builder = $this->select()->where(['users.id' => $id])->first();
        return $builder->first();
    }
    /*
     * todo: remove clients and external keys
     * todo: add price for stocks
     * todo: create view for stocks and btns buy/sell
     * todo: top up balance
     * */
}
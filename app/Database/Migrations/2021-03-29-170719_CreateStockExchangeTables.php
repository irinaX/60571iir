<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateStockExchangeTables extends Migration
{
	public function up()
	{
	    /*
	     * Так как в представлении используется таблица limit_order в которой присутствуют
	     * внешние ключи client_id, ticker_id, то необходимо так же создать таблицы client и ticker.
	    */
        // Drop table 'client' if it exists
        $this->forge->dropTable('client', true);
        // Table structure for table 'client'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'fullname' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('client');

        // Drop table 'ticker' if it exists
        $this->forge->dropTable('ticker', true);
        // Table structure for table 'ticker'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
            'img_url' =>[
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('ticker');

        // Drop table 'limit_order' if it exists
        $this->forge->dropTable('limit_order', true);
        // Table structure for table 'limit_order'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ticker_id' => [
                'type'       => 'INT',
                'unsigned'   => true,
                'null'       => false,
            ],
            'client_id' => [
                'type'       => 'INT',
                'unsigned'   => true,
                'null'       => false,
            ],
            'order_type' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
            'amount' => [
                'type'       => 'INT',
                'null'       => false,
            ],
            'price' => [
                'type'       => 'DECIMAL',
                'constraint' => '10,2',
                'null'       => false,
            ],
            'date_time' => [
                'type'       => 'DATETIME',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('ticker_id', 'ticker', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->addForeignKey('client_id', 'client', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->createTable('limit_order');
	}

	public function down()
	{
        $this->forge->dropTable('client', true);
        $this->forge->dropTable('ticker', true);
        $this->forge->dropTable('limit_order', true);
	}
}

<?php


namespace App\Models;

use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $table = 'client'; //таблица, связанная с моделью

    public function getClient()
    {
        $builder = $this->findAll();
//        $builder =  $this->select('*, limit_order.id')
//            ->join('ticker', 'limit_order.ticker_id = ticker.id', 'LEFT');
        return $builder;
    }
}
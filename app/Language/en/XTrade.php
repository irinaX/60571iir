<?php
return [
    'order_create_success' => 'Order successfully created.',
    'admin_permission_needed' => 'Access is denied.',
    'login_with_google' => 'Login with Google'
]

?>
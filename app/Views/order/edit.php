<?= $this->extend('templates/dashboard_layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <!--        <h2 class="mb-4">Edit order №--><?//= $order["id"] ?><!--</h2>-->
        <?= form_open_multipart('order/update'); ?>
<!--        --><?php //var_dump($order);?>
        <input type="hidden" name="id" value="<?= $order["id"] ?>">
        <div class="form-group">
            <label class="form-check-label mb-2" for="edit_ticker_name_list">Ticker</label>
            <input type="hidden"
                   id="edit_ticker_input"
                   name="ticker_id"
                   value="<?= $order["ticker_id"]; ?>"
            >
            <select class="form-select <?= ($validation->hasError('ticker_id')) ? 'is-invalid' : ''; ?>"
                    id="edit_ticker_name_list"
            >
                <?php foreach ($ticker as $item): ?>
                    <option value="<?= esc($item['id']); ?>">
                        <?= esc($item['name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('ticker_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="form-check-label mb-2" for="edit_client_name_list">Client</label>
            <input type="hidden"
                   id="edit_client_input"
                   name="client_id"
                   value="<?= $order["client_id"]; ?>"
            >
            <select class="form-select <?= ($validation->hasError('client_id')) ? 'is-invalid' : ''; ?>"
                    id="edit_client_name_list"
            >
                <?php foreach ($client as $item): ?>
                    <option value="<?= esc($item['id']); ?>">
                        <?= esc($item['fullname']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('client_id') ?>
            </div>
        </div>

        <div class="form-group">
            <label class="form-check-label mb-2">Type:</label>
            <div class="form-check ">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="order_type"
                           value="buy" <?= $order["order_type"] == 'buy' ? 'checked' : '' ?> >

                    <small class="form-text">buy</small>
                </label>
            </div>
            <div class="form-check ">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="order_type"
                           value="sell" <?= $order["order_type"] == 'sell' ? 'checked' : '' ?> >

                    <small class="form-text">sell</small>
                </label>
            </div>
            <div class="invalid-feedback" style="display: block">
                <?= $validation->getError('order_type') ?>
            </div>
        </div>
        <div class="form-group d-flex align-items-center">
            <label style="width: 120px;" for="amount">Amount:</label>
            <input type="text" class="form-control <?= ($validation->hasError('amount')) ? 'is-invalid' : ''; ?>"
                   name="amount"
                   value="<?= $order["amount"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('amount') ?>
            </div>
        </div>
        <div class="form-group d-flex align-items-center">
            <label style="width: 120px;" for="price">Price:</label>
            <input type="text" class="form-control <?= ($validation->hasError('price')) ? 'is-invalid' : ''; ?>"
                   name="price"
                   value="<?= $order["price"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('price') ?>
            </div>
        </div>
        <div class="form-group mb-4 ">
            <!--            <label for="date_time">date_time</label>-->
            <input type="datetime-local"
                   class="form-control <?= ($validation->hasError('date_time')) ? 'is-invalid' : ''; ?>"
                   name="date_time"
                   id="edit_date_time"
                   value="<?= $order["date_time"]; ?>"
            >
            <div class="invalid-feedback">
                <?= $validation->getError('date_time') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-secondary d-block mx-auto" name="submit">Edit</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?php
/*

        <div class="form-group d-flex align-items-center">
            <label style="width: 120px;" for="ticker_id">Ticker ID:</label>
            <input type="text" class="form-control <?= ($validation->hasError('ticker_id')) ? 'is-invalid' : ''; ?>"
                   name="ticker_id"
                   value="<?= $order["ticker_id"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('ticker_id') ?>
            </div>
        </div>
        <div class="form-group d-flex align-items-center">
            <label style="width: 120px;" for="client_id">Client ID:</label>
            <input type="text" class="form-control <?= ($validation->hasError('client_id')) ? 'is-invalid' : ''; ?>"
                   name="client_id"
                   value="<?= $order["client_id"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('client_id') ?>
            </div>
        </div>



*/

?>
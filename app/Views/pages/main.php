<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<section class="main-page">
    <div class="container">
        <div class="main-page__content">
            <h1 class="main-page__title">Welcome to <span>X</span>Trade!</h1>
            <p class="main-page__subtitle">Invest & save your money with small risk!</p>
            <div class="main-page__btn-group">
                <a class="btn btn-secondary" href="auth/register_user" role="button">register</a>
                <a class="btn btn-primary" href="auth/login" role="button">sign in</a>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>
